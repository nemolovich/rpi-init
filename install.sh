#!/bin/bash
# @author:  bgohier
# @date:    13/11/2016
##

cd /home/pi
echo "Begin initialization..."
set -x

cd ./rpi-init

sudo cp ./infra/.bashrc /etc/skel/
sudo cp ./infra/.bashrc ../
sudo cp ./infra/.bash_logout /etc/skel/
sudo cp ./infra/.bash_logout ../
sudo cp ./infra/.bash_prompt /etc/skel/
sudo cp ./infra/.bash_prompt ../
sudo cp ./infra/.profile /etc/skel/
sudo cp ./infra/.profile ../
sudo cp ./infra/rand-keygen /usr/bin/
sudo chmod +x /usr/bin/rand-keygen
sudo chmod +x ./docker/docker-init.sh

set +x
echo "Initialization done !"

# Update
sudo apt-get update
sudo apt-get install -y keyboard-configuration
sudo apt-get install -y raspi-config


## For VNC server:
# sudo apt-get install -y lxde
# sudo apt-get install -y tightvncserver
# sudo cp ./vnc/vncserver /usr/bin/vncserver

sudo apt-get install -y lynx

export VNC_PORT=25000


# Change hostname, internationalization and video memory max (16)
echo "
################################################################################
# WARNING:              User actions are required.                             #
################################################################################
# From the raspi-config interface (will appear after you press a key),         #
# You have to:                                                                 #
#          - Change the hostname                                               #
#          - Set the Internationalization                                      #
#          - Change the video memory used with 16                              #
#          - Expand filesystem                                                 #
#          - Choose boot option as console only                                #
################################################################################
"
read
sudo raspi-config
