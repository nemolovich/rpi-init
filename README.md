# Script to initialize the profile

## Modify Pirate
The default user created by Hypriot is pirate. We will modify this user to name back to pi.

Connect with ssh to rpi as pirate (default password: hypriot)

### Create "temp" user
You can not connect directy as root on the rpi. So we have to cretae another user in order
to modify the pirate user.

```sh
sudo passwd root
sudo passwd pirate
sudo adduser temp
```

Disonnect from ssh as pirate and connect as temp.

### Modify pirate user

```sh
su # to be as root
usermod -l pi pirate
groupmod --new-name pi pirate
usermod -m -d /home/pi pi
adduser pi sudo

sudo visudo
# In visudo, replace line:
#  %sudo   ALL=(ALL:ALL) ALL
# by:
#  %sudo   ALL=NOPASSWD: ALL

sudo chmod u+s `which ping`
```

Disonnect from ssh as temp and connect as pi.

### Remove temp user

```sh
sudo deluser temp --remove-home
sudo delgroup temp
```

## Infra config

```sh
export HOSTNAME="<RPI_NAME>" # RPI_NAME as rpi-0X with X pi number
old_host="$(hostname)"
sudo sed -i.bak 's/'${old_host}'/'${HOSTNAME}'/' /boot/device-init.yaml

sudo cp /boot/config.txt /boot/config.txt.bak
echo "
hdmi_force_hotplug=0
enable_uart=1

# camera settings, see http://elinux.org/RPiconfig#Camera
start_x=0
disable_camera_led=1
gpu_mem=16

" > ./config.txt
sudo chown root:root ./config.txt
sudo chmod 755 ./config.txt
sudo mv ./config.txt /boot/config.txt

sudo reboot
```

### Get the project from git

Retrieve the infrastructure project and install scripts.

```sh
cd ~
git clone https://bitbucket.org/capgemininantes/rpi-init.git
cd rpi-init
chmod +x ./install.sh
sudo ./install.sh
```

## Adding ssh keys on others servers

### Create authorized_keys file

The **authorized_keys** file contains the list of client public keys allowed to connect on the server.

Use following commands to create file:
```sh
mkdir ~/.ssh
touch ~/.ssh/authorized_keys
sudo chmod 600 ~/.ssh/authorized_keys
```

### Generate ssh key

Create a custom ssh key without password:

```sh
ssh-keygen -t rsa -b 2048
```

Leave empty for each question:

```diff
Generating public/private rsa key pair.
Enter file in which to save the key (/home/pi/.ssh/id_rsa):
- [Leave empty and press enter]
Created directory '/home/pi/.ssh'.
Enter passphrase (empty for no passphrase):
- [Leave empty and press enter]
Enter same passphrase again:
- [Leave empty and press enter]
Your identification has been saved in /home/pi/.ssh/id_rsa.
Your public key has been saved in /home/pi/.ssh/id_rsa.pub.
The key fingerprint is:
xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx pi@rpi-0X
The key's randomart image is:
+---[RSA 2048]----+
|         ..      |
|      . ..    .  |
|     . .  o  o   |
|      .  . o= E  |
|       .S o= O   |
|      .  o+ = o  |
|       .   + +   |
|        .   +    |
|         ...     |
+-----------------+
```

The ssh private key has been created in `~/.ssh/id_rsa` and the public key in `~/.ssh/id_rsa.pub`

Then you have to copy ssh plublic key on each others server in the `~/.ssh/authorized_keys` file:

```sh
# No user information because the user is the same on remote RPI
cat ~/.ssh/id_rsa.pub | ssh rpi-0X "sudo cat >> ~/.ssh/authorized_keys"
# Other possible command:
# ssh-copy-id -i ~/.ssh/id_rsa.pub rpi-0X
```

# Docker initialization

For the docker initialization go to [Docker session](https://bitbucket.org/capgemininantes/rpi-init/raw/master/docker/)