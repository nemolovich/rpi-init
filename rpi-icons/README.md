# Extras

## Icons

This is some extras for SD card.

Copy this folder on the SD root from windows.
Copy the **autorun.inf** and **rpi-X.ico** in the SD root (X the rpi number).
Modify the **autorun.inf** with correct name and icon.

## Tools

Tools to download:
- **VNC-Viewer-5.3.2-Windows-64bit**: VNC client
- **Win32DiskImager-0.9.5-install**: Flash SD card

## Image

Download the Hypriot OS image at: http://blog.hypriot.com/downloads/

(Current version: 1.1.0).

Use Win32DiskImager to flash image.
