# Initialize Docker Swarm

## Update the configuration file

The configuration file is placed in `/home/pi/rpi-init/docker/docker_properties.cfg` on the RPI.

### Set the IPs

Enter all the network RPI IPs (update with current IPs):
```sh
RPI1_IP="192.168.1.78"
RPI2_IP="192.168.1.79"
RPI3_IP="192.168.1.80"
RPI4_IP="192.168.1.81"
```

### Update network

Launch the following command:
```sh
/home/pi/rpi-init/docker/update-network.sh -u
```

## Initialize SWARM

Install bc package (basic calculator):

```sh
sudo apt-get install -y bc
```

Add user to docker group:

```sh
sudo gpasswd -a pi docker
```
