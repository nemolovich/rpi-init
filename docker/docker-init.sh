#!/bin/bash
# @author:  bgohier
# @date:    13/11/2016
##

HOME_DOCKER="/home/pi/rpi-init/docker"
cd ${HOME_DOCKER}

# Docker configs
new_opts=" --dns 192.168.1.1 --iptables=false"
sudo sed -i.bak 's/^#DOCKER_OPTS="\(.\+\)"$/DOCKER_OPTS="\1'"${new_opts}"'"/' /etc/default/docker
echo "---------------"
echo "New conf: $(cat /etc/default/docker)"
echo "---------------"
sudo systemctl restart docker

echo "---------------"
docker info
echo "---------------"
docker-compose -v
echo "---------------"
docker pull resin/rpi-raspbian:jessie

git clone http://bitbucket.org/capgemininantes/rpi-docker.git

cd ${HOME_DOCKER}/rpi-docker

get_tagged_images()
{
	echo $(sudo docker images -a --format "{{.Repository}}" | grep -v '<none>')
}

is_in_array()
{
	( echo ",$1," | sed -e 's/ /,/g' | grep -qe ",$2," ) && res=true || res=false
	echo ${res}
}

MAX_TRIES=5
build_image()
{
	tag_name="$1"
	path="$2"
	tries=0
	$(is_in_array "$(get_tagged_images)" "${tag_name}") && exists=true || exists=false
	if ${exists} ; then
		echo "Image '${tag_name}' already exists"
		return 0
	fi
	echo "Construct image '${tag_name}' from '${path}/Dockerfile'..."
	built=false
	while ! ${built} ; do
		sudo docker build -t ${tag_name} ${path}
		if [ $? -ne 0 ] ; then
			echo "An error occurred while building image '${tag_name}'"
			echo "Restarting service..."
			sudo systemctl restart docker
		else
			built=true
		fi
		tries=$((tries+1))
		if [ ${tries} -ge ${MAX_TRIES} ] ; then
			echo "Failed after ${tries} tries..."
			return 1
		fi
	done
}

DEBUG=false
debug()
{
	if ${DEBUG} ; then
		echo $*
	fi
}

hub_suffix="capgemininantes/rpi-"
directories="$(echo $(ls -d ./*/ | sed -e 's!''\.\|/!''!g'))"
debug "Directories: ${directories}"

ordered=""

for dir in ${directories} ; do
	name="$(echo ${dir})"
	debug "Name: ${name}"
	parent=$(cat ./${name}/Dockerfile | grep -i FROM | sed -e 's/FROM\s\+\([^:]\+\)\(:.\+\)\?/\1/ig')
	if [ ! -z "${parent}" ] ; then
		if ! $(is_in_array "$(get_tagged_images)" "${parent}") ; then
			short_parent="$(echo "${parent}" | sed -e 's/'"$(echo "${hub_suffix}" | sed -e 's!/!\\/!g')"'//g')"
			debug "Parent '${short_parent}' need to be build..."
			if $(is_in_array "${ordered}" "${name}") ; then
				debug "Child '${name}' already exists"
				ordered="$(echo "${ordered}" | sed -e 's/'${name}'/'"${short_parent} ${name}"'/g')"
			elif $(is_in_array "${directories}" "${short_parent}") ; then
				if $(is_in_array "${ordered}" "${short_parent}") ; then
					debug "Parent '${short_parent}' already present"
					ordered="${ordered} ${name}"
				else
					debug "Adding '${short_parent} ${name}'"
					ordered="${ordered} ${short_parent} ${name}"
				fi
			else
				debug "Check '${short_parent}' in '${directories}' $(is_in_array "${directories}" "${short_parent}")"
				debug "ERROR: Can not find parent '${short_parent}'"
			fi
		else
			ordered="${ordered} ${name}"
		fi
	else
		ordered="${ordered} ${name}"
	fi
	debug "Current: (${ordered})"
done

for dir in ${ordered} ; do
	build_image "${hub_suffix}${dir}" "./${dir}"
done

cd ${HOME_DOCKER}
git clone http://bitbucket.org/capgemininantes/apacheds-image.git
cd ${HOME_DOCKER}/apacheds-image
git checkout release/rpi
build_image "${hub_suffix}apacheds" "./"

docker rm -vf $(docker ps -a --format "{{.Names}}") 2>/dev/null
sudo docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}") 2>/dev/null
