#!/bin/bash
# @author:  bgohier
# @date:    18/11/2016
##

HOME_DIR="/home/pi"

CONFIG_FILE="${HOME_DIR}/rpi-init/docker/docker_properties.cfg"
SWARM_IMAGE_NAME="capgemininantes/rpi-whoami"
SWARM_IMAGE_VERSION="latest"

USAGE="Usage: $0 <ACTION> [,PARAMETERS]
ACTIONS:
    -h|--help                       Display this help message
    -u|--update-hosts               Update /etc/hosts with RPI IPs
    -r|--restore-hosts              Restore original /etc/hosts
    -a|--add-keys                   Add SSH plublic key on other RPIs
    -c|--check-keys                 Check if the public key has been send to other
                                    RPIs
    -s|--swarm-init                 Initialize Swarm
    -l|--leave-swarm                Remove all swarm services (this action is 
                                    made automatically when swarm-init is called)
    -i|--image <IMAGE>:<VERSION>    Set the image to use with swarm (optionnal)
                                    by default using:
                                      ${SWARM_IMAGE_NAME}:${SWARM_IMAGE_VERSION}
    -t|--test-swarm                 Test the swarm
    -n|--nb-tests <NB_TESTS>        Number of tests (default = 1)
    -w|--without-swarm              Create containers only on current machine
    -C|--clean-containers           Remove created containers
INFORMATION
    The RPI IPs must be declared in the configuration file '${CONFIG_FILE}'
        as 'RPI[X]_IP' variable name (with [x] the RPI number).
        eg. RPI1_IP=172.27.0.1
"

add_ips()
{
	if [ -f /etc/hosts.bak ] ; then
		sudo mv /etc/hosts.bak /etc/hosts
	fi
	ip1="$1"
	ip2="$2"
	ip3="$3"
	ip4="$4"
	cat > ~/hosts <<EOF
$(cat /etc/hosts)

${ip1}    rpi-01
${ip2}    rpi-02
${ip3}    rpi-03
${ip4}    rpi-04

EOF
	sudo cp /etc/hosts /etc/hosts.bak
	sudo mv ~/hosts /etc/hosts
	sudo systemctl daemon-reload
	sudo systemctl restart networking
}

restore_ips()
{
	sudo mv /etc/hosts.bak /etc/hosts
	sudo systemctl daemon-reload
	sudo systemctl restart networking
}

check_keys()
{
	for x in $(seq 4) ; do
		h="rpi-0${x}"
		if [ "${h}" != "$(hostname)" ] ; then
			ssh ${h} "hostname"
		fi
	done
}

install_keys()
{
	for x in $(seq 4) ; do
		h="rpi-0${x}"
		if [ "${h}" != "$(hostname)" ] ; then
			ssh-copy-id -i ${HOME_DIR}/.ssh/id_rsa.pub ${h}
		fi
	done
}

check_ips()
{
	if [ -z "${RPI1_IP}" -o -z "${RPI2_IP}" -o -z "${RPI3_IP}" -o -z "${RPI4_IP}" ] ; then
		echo "IP configuration not found (RPI[X]_IP with [X] the pi number)"
		exit 3
	fi
}

check_repl_param()
{
	if [ -z "${NB_REPLICAS}" ] ; then
		echo "Can not fin property NB_REPLICAS in configuration file"
		exit 6
	fi
}

init_swarm()
{
	image_n="$1"
	image_v="$2"
	nb_repl=$3
	
	echo "Initialize Swarm..."
	docker swarm join-token -q worker >/dev/null 2>&1
	if [ $? -eq 0 ] ; then
		leave_swarm
	fi
	
	docker swarm init >/dev/null 2>&1
	
	docker_folder=${HOME_DIR}/.docker
	mkdir -p ${docker_folder}
	export SWARM_TOKEN="$(docker swarm join-token -q worker)"
	echo "New Swarm token: ${SWARM_TOKEN}"
	echo "${SWARM_TOKEN}" > ${docker_folder}/.swarm_token
	chown -R pi:pi ${docker_folder}
	chmod 744 -R ${docker_folder}
	
	# docker-machine create -d generic \
			# --engine-storage-driver=overlay --swarm --swarm-master \
			# --swarm-image ${image_n}:${image_v} \
			# --swarm-discovery="token://${SWARM_TOKEN}" \
			# --generic-ip-address=$(getIP) \
			# $(hostname) &
	
	echo "Force other machines to join..."
	
	CURR_IP=$(hostname)
	make_manager=true
	for x in $(seq 4) ; do
		h="rpi-0${x}"
		if [ "${h}" != "$(hostname)" ] ; then
			ssh ${h} "mkdir -p ${docker_folder} ; \
			echo ${SWARM_TOKEN} > ${docker_folder}/.swarm_token ; \
			chown -R pi:pi ${docker_folder} ; \
			chmod 744 -R ${docker_folder}"
			ssh ${h} "docker swarm join --token ${SWARM_TOKEN} ${CURR_IP}:2377"
			if ${make_manager} ; then
				docker node update --role manager ${h}
				make_manager=false
			fi
		fi
	done
	
	# wait
	
	
	for x in $(seq 3) ; do
		p=$((8079+x))
		docker service create --name whoami${x} \
			-e WHOAMI_PORT=8080 -e CUSTOM_MESSAGE="[${x}] I'm \$hostname\$" \
			-p ${p}:8080 --replicas=$((nb_repl * x)) ${image_n}:${image_v}
	done

	echo "Initialization done"
	
}

leave_swarm()
{
	echo "Remove current Swarm..."
	for x in $(seq 4) ; do
		h="rpi-0${x}"
		if [ "${h}" != "$(hostname)" ] ; then
			ssh ${h} "docker swarm leave --force"
		fi
	done
	docker swarm leave --force
	echo "Cleaned"
}

uni_test()
{
	m_ip="$1"
	m_port=$2
	p_name="$3"
	nb_r=$4
	s=$(date +%s%N)
	r=$(echo $(curl http://${m_ip}:${m_port} 2>/dev/null) | sed -e 's/\r//g')
	e=$(date +%s%N)
	t=$(((e-s)/1000000))
	echo "${p_name}|${nb_r}|${r}|${t}"
}

test_swarm()
{
	nb=$1
	CURR_IP=$(hostname)
	RESULT_FILE="${HOME_DIR}/rpi-init/docker/rpi-docker/stats-volume/test_results"
	nb_services=$(docker service ls -q 2>/dev/null | wc -l)
	if [ ${nb_services} -eq 0 ] ; then
		RESULT_FILE="${RESULT_FILE}_no_swarm"
	fi
	rm "${RESULT_FILE}"
	echo "Starting tests..."
	for i in $(seq ${nb}) ; do
		for x in $(seq 3) ; do
			p=$((8079+x))
			s_name="whoami${x}"
			repls=$(docker service inspect --format='{{.Spec.Mode.Replicated.Replicas}}' ${s_name} 2>/dev/null)
			if [ -z "${repls}" ] ; then
				repls=0
			fi
			uni_test "${CURR_IP}" ${p} "${s_name}" ${repls}  >> "${RESULT_FILE}" &
		done
	done
	wait
	echo "Tests finished (result in ${RESULT_FILE})"
}

create_containers()
{
	image_n="$1"
	image_v="$2"
	for x in $(seq 3) ; do
		p=$((8079+x))
		s_name="whoami${x}"
		docker rm -vf ${s_name} 2>/dev/null
		docker run --name ${s_name} -d -e WHOAMI_PORT=8080 \
			-e CUSTOM_MESSAGE="[${x}] I'm \$hostname\$" -p ${p}:8080 ${image_n}:${image_v}
	done
}

remove_containers()
{
	for x in $(seq 3) ; do
		s_name="whoami${x}"
		docker rm -vf ${s_name} 2>/dev/null
	done
}

check_actions()
{
	if [ ! -z "${ACTION}" ] ; then
		echo "Error: can not use multiple actions"
		echo "${USAGE}"
		exit 5
	fi
}

UPDATE_ACTION=0
RESTORE_ACTION=1
SEND_KEYS_ACTION=2
CHECK_KEYS_ACTION=4
INIT_SWARM_ACTION=8
DEL_SWARM_ACTION=16
TEST_SWARM_ACTION=32
NO_SWARM_ACTION=64
DEL_CONTAINERS_ACTION=128
ACTION=

NB_TESTS=1

pre_load()
{
	while [ ! -z "$1" ] ; do
		case "$1" in
			-h|--help)
				echo "${USAGE}"
				exit 0
				;;
			-u|--update-hosts)
				check_actions
				ACTION=${UPDATE_ACTION}
				;;
			-r|--restore-hosts)
				check_actions
				ACTION=${RESTORE_ACTION}
				;;
			-a|--add-keys)
				check_actions
				ACTION=${SEND_KEYS_ACTION}
				;;
			-c|--check-keys)
				check_actions
				ACTION=${CHECK_KEYS_ACTION}
				;;
			-s|--swarm-init)
				check_actions
				ACTION=${INIT_SWARM_ACTION}
				;;
			-l|--leave-swarm)
				check_actions
				ACTION=${DEL_SWARM_ACTION}
				;;
			-t|--test-swarm)
				check_actions
				ACTION=${TEST_SWARM_ACTION}
				;;
			-w|--without-swarm)
				check_actions
				ACTION=${NO_SWARM_ACTION}
				;;
			-C|--clean-containers)
				check_actions
				ACTION=${DEL_CONTAINERS_ACTION}
				;;
			-n|--nb-tests)
				if [ -z "$2" ] || ! echo "$2" | grep -qe '[0-9]\+' ; then
					echo "Error: Please provide the number of tests to do"
					exit 8
				fi
				NB_TESTS=$2
				shift
				;;
			-i|--image)
				if [ -z "$2" ] || ! echo "$2" | grep -qe '\w\+:\w\+' ; then
					echo "Error: Please provide the image name and version"
					exit 7
				fi
				IMAGE="$2"
				;;
			*)
				echo "Error: Unknown parameter '$1'"
				echo "${USAGE}"
				exit 4
				;;
		esac
		shift
	done
}

pre_load $*

if [ ! -f "${CONFIG_FILE}" ] ; then
	echo "Error: can not find properties file '${CONFIG_FILE}'"
	exit 2
fi

. ${CONFIG_FILE}

if [ -z "${ACTION}" ] ; then
	echo "${USAGE}"
	exit 1
fi

case ${ACTION} in
	${UPDATE_ACTION})
		check_ips
		add_ips "${RPI1_IP}" "${RPI2_IP}" "${RPI3_IP}" "${RPI4_IP}"
		;;
	${RESTORE_ACTION})
		restore_ips
		;;
	${SEND_KEYS_ACTION})
		install_keys
		;;
	${CHECK_KEYS_ACTION})
		check_keys
		;;
	${INIT_SWARM_ACTION})
		check_repl_param
		init_swarm ${SWARM_IMAGE_NAME} ${SWARM_IMAGE_VERSION} ${NB_REPLICAS}
		;;
	${DEL_SWARM_ACTION})
		leave_swarm
		;;
	${TEST_SWARM_ACTION})
		test_swarm ${NB_TESTS}
		;;
	${NO_SWARM_ACTION})
		create_containers ${SWARM_IMAGE_NAME} ${SWARM_IMAGE_VERSION}
		;;
	${DEL_CONTAINERS_ACTION})
		remove_containers
		;;
	*)
		;;
esac
